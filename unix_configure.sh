read -p "Enter the project name: " name
read -p "Enter the timestamp field number (counting from 0): " ts
read -p "Enter the latitude field number (counting from 0): " lat
read -p "Enter the longitude field number (counting from 0): " lon
read -p "Enter the altitude field number (counting from 0): " alt
echo ""
echo "$name
$ts
$lat
$lon
$alt" > .project_config
