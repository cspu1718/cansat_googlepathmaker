#pragma once

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;


class GooglePathMaker {
	public:
		struct record{
			string ts;
			string GPS_Measures;
			string GPS_Latitude;
			string GPS_Longitude;
			string GPS_Altitude;
			string altitude;
		};
	
		void begin(int ts, int lat, int lon, int alt);
		void run(long long takeoff, long long landing, string team);
		
	private:
		void mySplit(string s, vector<string> &output, char splitter = ',');
		void getInputFile();
		void getOutputFile();
		bool verifyFile(string dir);
		void extractData(vector<string> rawData, int ts, int lat, int lon, int alt);
		void transferContent(ifstream &in, ofstream &out);
		double stod(string s);
		
		string inputFileDir = "input/";
		string inputFile;
		vector<string> rawInput;
		
		vector<record> data;
		
		string outputFileDir = "output/";
		string outputFile;
		
};
